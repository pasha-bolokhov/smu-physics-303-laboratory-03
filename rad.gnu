
print "========================================"
#
# In-coming pion
#

Ax = 1.144
Ay = -11.9

Bx = 2.48
By = -18.4

Cx = 5.71
Cy = -23.84

#
# calculation
#

P = 2 * (Bx - Ax)
Q = 2 * (By - Ay)
R = 2 * (Cx - Ax)
S = 2 * (Cy - Ay)

D = P * S - R * Q

M = Bx**2 - Ax**2  +  By**2 - Ay**2
N = Cx**2 - Ax**2  +  Cy**2 - Ay**2

x_pi_i = (M * S - Q * N) / D
y_pi_i = (P * N - R * M) / D
r_pi_i = sqrt( (Ax - x_pi_i)**2 + (Ay - y_pi_i)**2 )

print "in-coming pion"
print "x_pi_i = ", x_pi_i
print "y_pi_i = ", y_pi_i
print "r_pi_i = ", r_pi_i
print ""


#
# Out-going pion
#

Ax = 1.5
Ay = -11.62

Bx = 5.42
By = -14.6

Cx = 6.58
Cy = -16.7

#
# calculation
#

P = 2 * (Bx - Ax)
Q = 2 * (By - Ay)
R = 2 * (Cx - Ax)
S = 2 * (Cy - Ay)

D = P * S - R * Q

M = Bx**2 - Ax**2  +  By**2 - Ay**2
N = Cx**2 - Ax**2  +  Cy**2 - Ay**2

x_pi_o = (M * S - Q * N) / D
y_pi_o = (P * N - R * M) / D
r_pi_o = sqrt( (Ax - x_pi_o)**2 + (Ay - y_pi_o)**2 )

print "out-going pion"
print "x_pi_o = ", x_pi_o
print "y_pi_o = ", y_pi_o
print "r_pi_o = ", r_pi_o
print ""


#
# Out-going proton
#
Ax = -3.26
Ay = -4

Bx = -6.764
By = -0.7

Cx = 1.055
Cy = -11.1

#
# calculation
#

P = 2 * (Bx - Ax)
Q = 2 * (By - Ay)
R = 2 * (Cx - Ax)
S = 2 * (Cy - Ay)

D = P * S - R * Q

M = Bx**2 - Ax**2  +  By**2 - Ay**2
N = Cx**2 - Ax**2  +  Cy**2 - Ay**2

x_p_o = (M * S - Q * N) / D
y_p_o = (P * N - R * M) / D
r_p_o = sqrt( (Ax - x_p_o)**2 + (Ay - y_p_o)**2 )

print "out-going proton"
print "x_p_o = ", x_p_o
print "y_p_o = ", y_p_o
print "r_p_o = ", r_p_o
print ""

print "========================================"
s = (4.888 - 2.78) / 0.1
print "scale = ", s
print ""
print "R-incoming-pion =\t", r_pi_i / s
print "R-outgoing-pion =\t", r_pi_o / s
print "R-outgoing-proton =\t", r_p_o / s
print ""
