%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}
\usepackage{mdframed}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




\renewcommand{\theequation}{\arabic{equation}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{fashionfuchsia} Laboratory III  }\\[2mm]
{  \Large \textbf{\textit{\color{blue-violet} Conservation of Four--Momentum}}  }\\[2mm]
{  \large \it \color{deeppink} Modern Physics --- Physics 303  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                    G O A L                                   %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Goal}}

	The objectives of the laboratory are
\begin{itemize}
\item
	explore the difference between the newtonian and relativistic momentum

\item
	learn about some fundamental particle physics

\item
	determine whether newtonian or relativistic momentum is conserved in collisions
	of particles moving at high speeds
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	During the 1960's and 1970's the so--called \emph{bubble chambers} were used
	to observe the tracks and collisions of particles.
	The bubble chambers were big vessels typically filled liquid hydrogen which was
	brought into the superheated state.
	When a charged particle moved through the liquid hydrogen --- it left a trail of tiny bubbles.
	Physicists used photographs of such bubble tracks to study the collisions of particles.
	In modern days, of course, physicists use particle detectors that are more easily interfaced with computers.
	Nevertheless, in this laboratory we will be studying an image taken from a bubble chamber photograph
	of a collision between a particle called a \emph{negative pion} \cc{ \pi^- }
	and a hydrogen nucleus (which is just a \emph{proton} \cc{ p^+ }).
	The goal is to check whether this particular collision more closely satisfies the newtonian model
	or the relativistic model of momentum and energy conservation


\newpage
\newcounter{activity}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%         E X P E R I M E N T A L   R A D I I   O F   C U R V A T U R E        %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{activity}
\section*{\textit{Part \Roman{activity} --- Experimental Radii of Curvature}}

	A full image of a bubble chamber photograph taken at the Lawrence/Berkeley Laboratory will be shown to you.
	A bubble chamber was put at the end of a particle accelerator that was producing a highly energetic beam
	of negative pions (\cc{ \pi^- }).
	These pions are creating the slightly curved tracks that are streaking from the bottom of the image to the top.
	At various places in the photograph, you can see consequences of a variety of different interactions that take place.
	With so much happening in a single image though, it is sometimes hard to pick out and identify the interactions
	you are interested in.
	This is why we have taken a single collision and reproduced it on the figure on the last page of this laboratory.
	This single event shows a nearly elastic collision between a \cc{ \pi^- } and a \cc{ p^+ } at rest.
	The curved tracks show that the particles are charged,
	and the direction that the tracks curve can be used to identify the charge of each particle

\bigskip\noindent
	Why are the tracks curved?
	This is because the bubble chamber was placed in a magnetic field.
	Since the magnetic field curves particles with opposite charges in opposite directions,
	we can identify the \emph{sign of the charge} moving.

\bigskip\noindent
	It can be shown that a particle with charge \cc{ q } and mass \cc{ m } moving
	perpendicular to a uniform magnetic field follows a circular path with radius \cc{ R }, given by:
\ccc
\beq
%
\label{R}
	R	~~=~~	\frac{ p }{ |q|\, B }
	\ccb\,.
	\ccc
\eeq
\ccb
	In this equation \cc{ p } is the particle's momentum, which involves its mass and speed.
	Therefore, if we know \cc{ R }, \cc{ q } and \cc{ B }, we can in principle determine its momentum \cc{ p }.
	Then if we know its mass \cc{ m }, we can find its speed.
	If we use equation \eqref{R} as it is, we could often conclude that a particle was travelling faster than the speed of light.
	In reality, \cc{ p } is the \emph{four--momentum} of the particle.
	While the difference would be negligible for normal speeds,
	for a particle in a collider it matters.
	The necessary step is to measure the radius of curvature of the track, though


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                           C I R C L E   T H E O R Y                          %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{activity}
\section*{\textit{Part \Roman{activity} --- Circle Theory}}

	Each track is gently curved, so it may at first seem very difficult to measure its radius of curvature.
	However, consider the diagram below. If we only have a portion of a circular track between points \cc{ A } and \cc{ B },
	we can still find the distance to circle's center at \cc{ O }.
	If we measure the length of the chord \cc{ w } along a straight line between \cc{ A } and \cc{ B },
	and the height \cc{ h } of the curve above the midpoint of the chord,
	we can use the Pythagorean theorem on the right triangle \cc{ \Delta O A C } shown in the diagram

\bigskip\noindent
	Using this information, one can show\footnote{This should be done at some point} that

\ccc
\beq
%
\label{Rexp}
	R	~~=~~	\frac{w^2}{8\,h}  ~~+~~  \frac h 2	~~\approx~~	\frac{w^2}{8\,h}
	\ccb\,,
	\qquad\qquad\qquad \text{when \cc{ 2 h ~\ll~ w }}
	\ccb\,.
	\ccc
\eeq
\ccb
	(\emph{hint:} you do not \emph{have} to use the approximation, though ---
	you can just use the full equation if you are concerned that \cc{ 2 h } is not much less than \cc{ w })

\begin{center}
\begin{tikzpicture}
	% geometry definitions
	\def \R {8}
	\def \CircleStretchAngle {30}
	\def \TriangleAngle {24}
	\node (O) at (0, 0) {};
	\node (A) at (90 + \TriangleAngle: \R) {};
	\node (B) at (90 - \TriangleAngle: \R) {};
	\node (C) at (A.east -| O.north) {};

	% circle
	\draw	[draw = jasper, very thick, line cap = round]
		(90 + \CircleStretchAngle: \R)	arc	[radius = \R, start angle = 90 + \CircleStretchAngle, end angle = 90 - \CircleStretchAngle];

	% points and lines
        \draw   [ultra thick, draw = ProcessBlue]
		(A.center)	circle  [radius = 0.3mm]
		node    	[shift = {(-2mm, 3mm)}] {\cc{ A }};
	\draw	[thick, draw = ProcessBlue]
		(A.center)	--	(B.center)
		node		[pos = 0.25, below]	{\cc{ \frac 1 2 w }}
		node		[pos = 0.75, below]	{\cc{ \frac 1 2 w }};
        \draw   [ultra thick, draw = ProcessBlue]
		(B.center)	circle	[radius = 0.3mm]
		node		[shift = {(2mm, 3mm)}] {\cc{ B }};
	\draw	[thick, draw = ProcessBlue]
		(O.center)	--	(90: \R);
	\draw	[ultra thick, draw = ProcessBlue]
		(O.center)	circle	[radius = 0.3mm]
		node		[shift = {(-1mm, -3mm)}] {\cc{ O }};
	\draw	[ultra thick, draw = ProcessBlue]
		(C.center)	circle	[radius = 0.3mm]
		node			[shift = {(-3mm, -3mm)}] {\cc{ C }};
	\draw	[thick ,draw = ProcessBlue]
		(O.center)	--	(A.center);
	\draw	[thick ,draw = ProcessBlue]
		(O.center)	--	(B.center)
		node			[midway, shift = {(2mm, -2mm)}]	{\cc{ R }};

	% lengths
	\draw	[thin, draw = ProcessBlue]
		($ (C) ! 0.5 ! (90: \R) $) + (0.8mm, 0)	--	++(40: 1.2cm)
							--	++(0.4cm, 0)
		node						[midway, above]	{\cc{ h }};
\end{tikzpicture}
\end{center}

\noindent
	This is how we can measure \cc{ R } for each of the tracks from our bubble chamber image


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                M O M E N T U M                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{activity}
\section*{\textit{Part \Roman{activity} --- Momentum and Particle Physics}}

	In Special Relativity it is convenient to express speeds in units of the speed of light,
	while in classical mechanics we use the SI units.
	Which units are we going to use now?
	In particle physics, we use other units, which might seem unusual at first sight.
	They are based on the unit of \emph{electron--volt} (\cc{ \si\eV }),
\ccc
\beq
\label{eV}
	\SI{1}\eV	~~=~~	e \cdot \SI{1}\volt	~~=~~	\SI{1.602d-19}\joule
	\ccb\,,
	\ccc
\eeq
\ccb
	where \cc{ e } is absolute value of the charge of the electron.
	For practical purposes, though, it is more convenient to use the units of \emph{mega--electron--volts},
\ccc
\beq
\label{MeV}
	\SI{1}\MeV ~=~ \SI{d6}\eV
	\ccb\,.
	\ccc
\eeq
\ccb
	Here are the basic units of particle physics\footnote{
		\cc{ \si\GeV } is used more often than \cc{ \si\MeV },
		and constants \cc{ \hbar } and \cc{ c } are frequently omitted altogether,
		although they are implied},
\ccc
\begin{align}
%
\notag
	[\,E\,]	& ~~=~~	\SI{1}\MeV	&	[\,l\,]	& ~~=~~	\SI{1}{\hbar c/\MeV}
	\\[4mm]
%
\label{units}
	[\,p\,]	& ~~=~~	\SI{1}{\MeV/c}	&	[\,t\,]	& ~~=~~	\SI{1}{\hbar/\MeV}
	\\[4mm]
%
\notag
	[\,m\,]	& ~~=~~	\SI{1}{\MeV/c\squared}
\end{align}
\ccb
	These units seem unusual,
	but you can really think of them as just lengthy ``symbols'', without trying to interpret what they read.
	In order to convert between units \eqref{units} and the SI system, one has to literally plug in
	the SI values of \cc{ e }, \cc{ c } and \cc{ \hbar } into them, including \emph{their} units.
	But things are simpler than they seem.
	In particular, we are not going to need the particle physics units for \cc{ [\,l\,] } and \cc{ [\,t\,] }
	in this laboratory

\bigskip\noindent
	It turns out, that as long as one uses energy in \cc{ \si\MeV }, momentum in \cc{ \si{\MeV/c} } and
	mass in \cc{ \si{\MeV/c\squared} }, one does not have to take care of speed of light.
	In other words, the speed will be measured in the units of speed of light, and symbol \cc{ c } drops out from numerous
	calculations and derivations (although it does show up in the units).
	The mass of electron is \cc{ m_e ~=~ \SI{0.511}{\MeV/c^2} }, mass of pion is \cc{ m_\pi ~=~ \SI{139.6}{\MeV/c^2} },
	while that of the proton is \cc{ m_p ~=~ \SI{938.3}{\MeV/c^2} } ---
	these numbers are a lot more reasonable than if we wrote them in \cc{ \si\kg }

\bigskip\noindent
	Equations \eqref{eV} and \eqref{MeV} tell us how to convert \emph{energy} from joules to MeV.
	But the radius of curvature equation \eqref{R} gives us the \emph{momentum} instead of energy,
\ccc
\beq
\label{p}
	p	~~=~~	|q|\, B\, R
	\ccb\,.
	\ccc
\eeq
\ccb
	How do we convert that?
	Let us see, the SI units for momentum can be written as \cc{ \si{\joule\per(\m/\s)} }, and so
\ccc
\begin{align}
%
\notag
	1\,\frac{\si\joule}{\si{\m/\s}}	& ~~=~~	\frac{1}{\num{1.6d-19}}\,\frac{\eV}{\si{\m/\s}}
					~~=~~	\frac{1}{\num{d6} \times \num{1.6d-19}}\,\frac{\si\MeV}{\si{\m/\s}}
					~~=~~
	\\[2mm]
%
					& ~~=~~	\frac{\num{3d8}}{\num{d6} \times \num{1.6d-19}}\,\frac{\si\MeV}{\SI{3d8}{\m/\s}}
					~~=~~	\frac{1}{\num{1.6d-19}} \times \SI{300}{\MeV/c}
	\ccb\,.
	\ccc
\end{align}
\ccb
	In other words,
\ccc
\beq
\label{punits}
	1\,\frac{\si\joule}{\si{\m/\s}}	~~=~~	\frac{1}{\num{1.6d-19}} \times \SI{300}{\MeV/c}
	\ccb\,.
	\ccc
\eeq
\ccb
	We have not simplified the numbers in \eqref{punits} further
	and left the electron's charge value \cc{ \num{1.6d-19} } in the denominator on purpose,
	since charge \cc{ q } in \eqref{p} will always be an integral multiple of this number.
	If we plug in the values into equation \eqref{p}, without multiplying them ---
	we will see that the electron's charge cancels out.
	Using \eqref{punits} requires an even \emph{simpler} calculation of momentum than it would be in the classical units,
\ccc
\beq
\label{p-ready}
	p	~~=~~	\frac{ |q| }{ \num{1.6d-19} }\,\, B\, R\, \cdot \SI{300}{\MeV/c}
	\ccb\,,
	\ccc
\eeq
\ccb
	assuming \cc{ q }, \cc{ B } and \cc{ R } are just the values given \emph{in the SI units}.
	The unit names --- \cc{ \si\m }, \cc{ \si\tesla } and \cc{ \si\coulomb } \emph{should not be included} in this calculation,
	as they are implicitly present in \cc{ \si{\MeV/c} } --- only the respective numbers should be plugged in.
	You will do the actual calculations later

\bigskip\noindent
	From this point onwards, we want to find the newtonian momentum \cc{ \wt p } and the newtonian kinetic energy
	\cc{ \wt K }, and see if these are conserved in the collision.
	First, we need to find the velocity \cc{ v } (in units of speed of light).
	Remind, that the magnitude \cc{ p } of the four-momentum is,
\ccc
\beq
\label{p-v}
	p	~~=~~	\frac{ m\, v }{ \sqrt{ 1 ~-~ v^2 } }
	\ccb\,.
	\ccc
\eeq
\ccb
\vspace*{-6mm}
\begin{mdframed}
	[linecolor = alizarin, topline = false, bottomline = false, rightline = false,
	rightmargin = 0, innerrightmargin = 0, innerbottommargin = -4mm,
	innerleftmargin = 8mm, linewidth = 0.6mm]
	Solve Eq.~\eqref{p-v} for \cc{ v } (using a separate sheet of paper), and show that
\ccc
\beq
	v	~~=~~ \frac{ p }{ \sqrt{ m^2 ~+~ p^2 } }
	\ccb\,.\ccc
\eeq
\ccb
\end{mdframed}
\vspace*{-6mm}
	The newtonian momentum \cc{ \wt p } is then
\ccc
\beq
\label{non-rel-p}
	\wt p	~~=~~	m\,v	~~=~~	\frac{ m\, p }{ \sqrt{ m^2 ~+~ p^2 } }
	\ccb\,,\ccc
\eeq
\ccb
	and the newtonian kinetic energy
\ccc
\beq
\label{non-rel-K}
	\wt K	~~=~~	\frac 1 2\, m\, v^2	~~=~~	\frac{ \wt p{}^2 }{2\, m}
	\ccb\,.\ccc
\eeq
\ccb
	If you use momentum \cc{ p } in \cc{ \si{\MeV/c} } and mass \cc{ m } in \cc{ \si{\MeV/c^2} },
	the non--relativistic momentum \cc{ \wt p } and kinetic energy \cc{ \wt K } will also
	end up in those units, respectively.
	Equations \eqref{p-ready}, \eqref{non-rel-p} and \eqref{non-rel-K} will be helpful for us
	when examining the tracks from the bubble chamber


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                       C O O R D I N A T E   S Y S T E M                      %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{activity}
\section*{\textit{Part \Roman{activity} --- Defining a Coordinate System}}

	We have only solved equation \eqref{non-rel-p} for the magnitude of the momentum of the particle
	with the algebra of the previous page.
	We have not yet solved for the components of the momentum.
	Since it is the \emph{components} of the momentum (or four--momentum) that are conserved,
	we will need to be able to find them

\bigskip\noindent
	For each particle, you will need to calculate the \cc{ x }-- and \cc{ y }--components of its momentum.
	Draw lines that show the angle each particle makes just after the collision.
	Notice how we have conveniently chosen the \cc{ x }--axis to be the original \cc{ \pi^- } direction of motion
	just before the collision

\bigskip\noindent
	Measure your angle using a protractor.
	Note --- these tangent lines will \emph{not} be the same as the chords you will draw to compute \cc{ R }.
	You will need to draw new lines

\bigskip\noindent
	Your tracks are located in an \cc{ xy } plane.
	There is not a \cc{ z }--component to the motion, so you will not need to consider it

\begin{center}
\begin{tikzpicture}
	% x-axis
	\draw	[draw = ProcessBlue, thin, ->, >={Stealth[length = 1.8mm]}]
		(-3, 0)	--	(3, 0)
		node		[shift = {(2mm, -2mm)}] {\cc{ x }};

	% tracks
	\draw	[draw = jasper, very thick,
		postaction = {decorate, decoration = {markings, mark = at position 0.5 with {\arrow{Stealth[length = 2.8mm]}}}}]
		(-160: 3)	to [out = 50, in = -180]	(0, 0);
	\draw	[draw = jasper, very thick,
		postaction = {decorate, decoration = {markings, mark = at position 0.7 with {\arrow{Stealth[length = 2.8mm]}}}}]
		(0, 0)		to [out = 50, in = -90]		(70: 2.5);
	\draw	[draw = jasper, very thick,
		postaction = {decorate, decoration = {markings, mark = at position 0.7 with {\arrow{Stealth[length = 2.8mm]}}}}]
		(0, 0)		to [out = -30, in = 90]		(-60: 3.0);

	% lines
	\draw	[draw = ProcessBlue, thin]
		(0, 0)	--	(50: 2.8)
		(0, 0)	--	(-30: 2.8);
	\draw	[draw = ProcessBlue, thin, <->, > = {Stealth[length = 1.4mm]}]
		(0.8, 0)	arc	[radius = 0.8, start angle = 0, end angle = 50]
		node			[midway, shift = {(3mm, 1mm)}]	{\cc{ \theta_1 }};
	\draw	[draw = ProcessBlue, thin]
		(1.32, 0)	arc	[radius = 1.32, start angle = 0, end angle = -30];
	\draw	[draw = ProcessBlue, thin, <->, > = {Stealth[length = 1.4mm]}]
		(1.4, 0)	arc	[radius = 1.4, start angle = 0, end angle = -30]
		node			[midway, shift = {(3mm, -1mm)}]	{\cc{ \theta_2 }};
\end{tikzpicture}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            C A L C U L A T I O N S                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\refstepcounter{activity}
\newcounter{calculations}
\setcounter{calculations}{\value{activity}}
\section*{\textit{Part \Roman{activity} --- Calculations}}

	\emph{Each person} should do their own calculations in steps \ref{identify} to \ref{tangent} below
	on their diagram.
	When you have completed analysis of your diagram,
	you will need to use a computer to have it do all the relevant calculations for you
\begin{enumerate}[label=(\alph*)]
\item
\label{identify}
	Identify which outgoing particle is which by the way it curves

\item
	Draw the straight line chords stretching from end to end along each particle's path
	using a very fine pencil.
	Measure \cc{ w } and \cc{ h }
	(try to measure them as carefully as possible, especially \cc{ h })

\item
	Use \cc{ w } and \cc{ h } to determine \cc{ R }

\item
\label{tangent}
	Carefully draw tangent lines to the particles' paths just before or after the collision
	and measure the angles for the outgoing particles

\item
	Use \cc{ R } and the particles' mass to calculate the non--relativistic momentum \cc{ \wt p }
	using equation \eqref{non-rel-p} for each particle

\item
	Use your measured quantities to calculate the newtonian kinetic energy \cc{ \wt K } in \eqref{non-rel-K}
	and components of the total momentum \cc{ \wt p{}_x } and \cc{ \wt p{}_y } --- before and after the collision.
	Are these conserved?

\item
	If you find there is a problem with these being conserved,
	what is wrong with our analysis?
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                           F O U R - M O M E N T U M                          %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{activity}
\section*{\textit{Part \Roman{activity} --- Four--momentum}}

	Your calculations in Part \Roman{calculations} will most likely not show conservation of energy or momentum.
	As we have seen in Chapter 9 and 10 of the T.~Moore textbook,
	we need to use the four--momentum instead.
	The four--momentum of an individual particle is a four dimensional vector whose components are,
\ccc
\beq
%
\label{four-momentum}
	\lgr
	\begin{matrix}
		E	\\[6mm]
		p_x	\\[5mm]
		p_y	\\[5mm]
		p_z
	\end{matrix}
	\rgr
	~~=~~
	\lgr
	\begin{matrix}
		\sqrt{ m^2 ~+~ p^2 }							\\[3mm]
		\frac{ \displaystyle m\, v_x }{\displaystyle \sqrt{ 1 ~-~ v^2 }}	\\[5mm]
		\frac{ \displaystyle m\, v_y }{\displaystyle \sqrt{ 1 ~-~ v^2 }}	\\[5mm]
		\frac{ \displaystyle m\, v_z }{\displaystyle \sqrt{ 1 ~-~ v^2 }}
	\end{matrix}
	\rgr
	\ccb.
	\ccc
\eeq
\ccb

\noindent
	We call \cc{ p } the particle's relativistic momentum and \cc{ E } the particle's relativistic energy.
	Note that a particle has a relativistic energy \cc{ E ~=~ m } when it is at rest.
	Also, note that in all these expressions, the mass is a speed--independent constant
	(no matter what else you might have heard in other classes)

\bigskip\noindent
	Although this looks unwieldy, it is very simple to use in this case.
	We have already found the magnitude of the four--momentum \cc{ p } in Eq.~\eqref{p},
\ccc
\beq
\label{R-p}
	p	~~=~~	|q|\, B\, R
	\ccb\,.\ccc
\eeq
\ccb
	Do the same analysis as before, except check to see if the components of the four--momentum are conserved.
	If a particle makes an angle \cc{ \theta } with the \cc{ \mathbold{+\hat x} } direction
	(in the range from \cc{ -\ang{180} } to \cc{ +\ang{180} }),
	its four--momentum is simply
\ccc
\beq
%
\label{p-via-p}
	\lgr
	\begin{matrix}
		E	\\[6mm]
		p_x	\\[5mm]
		p_y	\\[5mm]
		p_z
	\end{matrix}
	\rgr
	~~=~~
	\lgr
	\begin{matrix}
		\sqrt{ m^2 ~+~ p^2 }		\\[3mm]
		\pm\, p\, \cos\,\theta		\\[5mm]
		\pm\, p\, \sin\,\theta		\\[5mm]
		0
	\end{matrix}
	\rgr
	\ccb.
	\ccc
\eeq
\ccb
	Add up components of the four--momentum as you would for three-vectors,
	but just realize that it happens to have an extra component

\bigskip\noindent
	Are the components of the four-momentum conserved?
	Of course, your answers will not be \emph{exactly} equal to each other
	(you made experimental measurements that have errors associated with them),
	but you should find that the four--momentum is more closely conserved than the newtonian momentum.
	If this was not the case, then you should look at your measurements and calculations again


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                       L A B O R A T O R Y   R E P O R T                      %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\stepcounter{activity}
\section*{\textit{Part \Roman{activity} --- Laboratory Report}}

	The laboratory report for this class will actually be a little different.
	Each person should write a description and explanation of the experiment you did in class for your parents
	or some other family member on the back of a postcard.
	You should draw pictures or diagrams on the front side of the postcard (a postcard does need a picture, right?).
	Write your postcard so that it does not take a particle physicist (even in the case they happen to be one) to understand it.
	You should use words that anyone would be able to understand, so try real hard to make your experiment understandable to them

\bigskip\noindent
	Address an envelope to the family member of your choice, and hand it in to the instructor to check out.
	Once it is inspected --- it will be mailed to you.
	Surely, your family member will be excited to hear from you and what you are doing here at Saint Martin's University


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                   P H O T O                                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
	\includegraphics[width=18cm]{pion-proton-collision.png}
%%
%%	Won't hurt to make the intersection point common
%%
%%	\begin{tikzpicture}[remember picture, overlay]
%%		\node	at (current page.center) {\includegraphics[width=18cm]{pion-proton-collision.png}};
%%	%
%%	%	in-coming pion
%%	%
%%	%	x = 20.6819472432754
%%	%	y = -11.2715000743052
%%	%	r = 19.54805347439
%%	%
%%		\draw	[draw = electricviolet, ultra thin]
%%			(20.6819472432754, -11.2715000743052)
%%			circle	[radius = 19.54805347439];
%%		\draw	[draw = electricviolet, ultra thin]
%%			(1.144, -11.9)	circle	[radius = 0.2mm];
%%		\draw	[draw = electricviolet, ultra thin]
%%			(2.48, -18.4)	circle	[radius = 0.2mm];
%%		\draw	[draw = electricviolet, ultra thin]
%%			(5.71, -23.84)	circle	[radius = 0.2mm];
%%	
%%	
%%	%
%%	%	out-going pion
%%	%
%%	%	x = -1.70744680851065
%%	%	y = -19.9074468085107
%%	%	r = 8.88647790934736
%%	%
%%		\draw	[draw = electricviolet, ultra thin]
%%			(-1.70744680851065, -19.9074468085107)
%%			circle	[radius = 8.88647790934736];
%%		\draw	[draw = electricviolet, ultra thin]
%%			(1.5, -11.62)	circle	[radius = 0.2mm];
%%		\draw	[draw = electricviolet, ultra thin]
%%			(5.42, -14.6)	circle	[radius = 0.2mm];
%%		\draw	[draw = electricviolet, ultra thin]
%%			(6.58, -16.7)	circle	[radius = 0.2mm];
%%	
%%	
%%	%
%%	%	out-going proton
%%	%	circle given by gnuplot
%%	%
%%	%	x = -21.6965562276175
%%	%	y = -20.065965158052
%%	%	r = 24.454485109972
%%	%
%%		\draw	[draw = deeppink, ultra thin]
%%			(-21.6965562276175, -20.065965158052)
%%			circle	[radius = 24.454485109972];
%%		\draw	[draw = deeppink, ultra thin]
%%			(-3.26, -4)	circle	[radius = 0.4mm];
%%	
%%		\draw	[draw = deeppink, ultra thin]
%%			(-6.764, -0.7)	circle	[radius = 0.4mm];
%%	
%%		\draw	[draw = deeppink, ultra thin]
%%			(1.055, -11.1)	circle	[radius = 0.2mm];
%%	
%%	
%%	%
%%	%	the scale
%%	%
%%		\draw	[draw = deeppink, ultra thin]
%%			(2.78, -4.704)	circle	[radius = 0.2mm];
%%		\draw	[draw = deeppink, ultra thin]
%%			(4.888, -4.704)	circle	[radius = 0.2mm];
%%	\end{tikzpicture}
\end{center}


\end{document}
